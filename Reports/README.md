# README

This folder contains a few Report examples

## Top Storage Consumer

The purpose of this report is to highlight CloudForms' capability to show data from different providers in one Report (in this example disk usage per VM). The second idea is to demonstrate the formatting capabilities by highlighting particularly large VMs.

* Top Storage Consumer.yaml: the actual report
* Top Storage Consumer widget.yaml: a widget which uses the report and can be used in a Dashboard

## Non compliant Container Images

Make sure to have SmartState Analysis performed on some container images, before using these reports (or they will not report any data):

* non-compliant-container-images-simplified.yaml: Report listing all container projects with non-compliant images
* non-compliant-container-images.yaml: Report listing all non-compliant container images

## Misc

* Top Storage Consumer.yaml: Report with all VMs and instances sorted by Storage consumption, good example to show how Reports can consolidate data over multiple providers
* Number of Snapshots.yaml: Reports the number of Snapshots per VM, the parent Provider and Cluster, often Snapshots become too large or there are too many which has negative impact on performance
* esx-advanced-settings-syslog.yml: This report will retrieve advanced settings from ESX hosts and filter "Syslog" settings. Smart State Analysis must have been executed at least once or this report will not show any data.
* Cost_saving_potential.yaml: Listing VMs using Right Size Recommendations, highlighting certain thresholds
* Health_status.yaml: Show status of Virtual Machines and highlight certain thresholds