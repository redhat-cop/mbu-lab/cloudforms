# Service Catalog

There are four Service Catalogs with multiple Service Catalog Items. A good introduction into managing Service Catalogs can be found in the [Building a self-service portal with Red Hat CloudForms](https://github.com/cbolz/summit-fy19/blob/master/self-service-portal-with-cloudforms/index.md) lab which was originally presented during Red Hat Summit in 2018.

- Applications
  - Wordpress on OpenStack
- Linux Workloads
  - RHEL 7 on OpenStack
  - RHEL 7 Centrally Managed
  - RHEL 7 Self Managed
  - RHEL 7 on VMware
- Windows
  - Windows 10
  - Windows 2008 R2 Server

:heavy_check_mark: ***NOTE*** Don't forget to also demonstrate the new Self Service UI accessible by using the URL:

    http://<your hostname>/self_service.

## Applications: Wordpress on OpenStack

This is a Service Catalog Item using the Wordpress HEAT Template originally found on the [OpenStack HEAT repository](https://github.com/openstack/heat-templates/tree/master/hot/software-config/example-templates/wordpress). Some fixes were applied and the version used in this demo can be found in the folder [HEAT](../HEAT/).

## Linux: RHEL 7 on OpenStack

This is a simple Service Catalog Item using an RHEL 7 Template to provision an instance on OpenStack.

An intentionally simple Service Dialog is used to show how a simple Service Dialog can look like.

## Linux: RHEL 7 Centrally Managed

This Service Catalog Item is using an RHEL 7 Template to provision a Virtual Machine on Red Hat Virtualization.

The Service Dialog will ask the user to choose a T-Shirt size, specify a user name and the public key for this user. This user will be created automatically during provisioning and the specify key will be added. The utilized Cloud-Init configuration can be found in the [../CloudInit/](../CloudInit/README.md) sub folder.

In this use case we demonstrate a scenario where the end user is not "root" on the Virtual Machine and it will be centrally managed, e.g. patches, backups and management is done by a different team.

## RHEL 7 Self Managed

This Service Catalog Item is using an RHEL 7 Template to provision a Virtual Machine on Red Hat Virtualization.

The Service Dialog will ask the user to choose a T-Shirt size and a public key which will be added to the authorized_keys for root. The utilized Cloud-Init configuration can be found in the [../CloudInit/](../CloudInit/README.md) sub folder.

In this use case we demonstrate a scenario where the end user will have "root" privileges on the Virtual Machine.

## Linux: RHEL 7 on vCenter

This is a simple Service Catalog Item using an RHEL 7 Template to provision a Virtual Machine on VMware vCenter.

This Service Catalog Item also demonstrates that CloudForms can indeed show estimated costs in a Service Catalog. This is a quite popular demand. The way this is implemented in this demo is very simple. There are three different T-Shirt sizes and depending on the select, the price estimated at the bottom is recalculated and updated. The code can be found in this repository in the [Automate sub folder](../Automate/). The costs are defined in [estimate_cost](https://gitlab.com/cjung/cloudforms-demo/blob/master/Automate/Integration/DynamicDialogs/methods.class/__methods__/estimate_cost.rb) method.

## Windows 10

This will deploy a Windows 10 Virtual Machine on VMware. With this Service Catalog Item, it is shown that CloudForms can also manage the latest version of Windows.

## Windows: Windows 2008 R2 Server

This is a simple Service Catalog Item using an evaluation version of Windows 2008 R2 Template to provision a Virtual Machine on VMware vCenter.

An intentionally simple Service Dialog is used to show how a simple Service Dialog can look like.