# UI Customization

A must for every customer facing demo is to upload and active the customer logo and a wallpaper. It is very easy to implement and always very well received.

Before you begin, find to suitable files and adjust them to the required format and dimension. You can use Gimp or any other image editor which can edit and save images in PNG format. Many companies provide officially licensed and optimized versions of their company logo and additional images which can be used as a wallpaper, on their web site. Sometimes it's under the company profile or the press material. As a last resort, you can always fall back to the [Google Image Search](https://images.google.com).

## Company Logo

It should be an image not larger than 350x70 pixels in PNG format. CloudForms does not stretch the logo to fit (aka it keeps the width and height ratio). Try to find a logo which looks good on a dark background. The logo will be seen on the login page and all pages (Classic UI and Self Service UI).

## Company Wallpaper

It should be an image not larger than 1200x1000 pixels in PNG format. CloudForms does not stretch the logo to fit (aka it keeps the width and height ratio). The width is usually more important than the height of the image, if you can't find one in the suitable dimension. Try to find a wallpaper which works well with a white font and the grey shade on the login page, since the font color can not be changed.

The wallpaper will only be shown on the login page of the Classic UI. The Self Service UI does currently not support it ([Bugzilla #1497726](https://bugzilla.redhat.com/show_bug.cgi?id=1497726))

## Configuration

There are a few simple steps to upload the customizations. To perform these actions, you have to be either an Super Administrator or the necessary configure rights.

1. Log into CloudForms

1. Click on your username on the top right and on ***Configuration***

    ![Open Configuration](./Images/configuration.png)

1. Click on ***Custom Logos*** in the window

    ![click on custom logos](./Images/custom-logos.png)

1. Click on ***Choose file*** and use the File Open Dialog to select the image

1. Click on ***Upload*** to upload the selected image

1. Enable the image

    ![Enable Company Logo](./Images/enable-logo.png)

    ![Enable Company Wallpaper](./Images/enable-wallpaper.png)

    :heavy_check_mark: ***NOTE*** Starting with CloudForms 4.7 (CFME 5.19), you'll see a third option ***Custom Brand Image*** which will replace the text "Red Hat CloudForms Management Engine" on the top left of the screen and on the login page, with the specified image.

1. Don't forget to click on ***Save***

The company logo should now already be seen on the top right, next to your username. Log out of CloudForms to verify the company wallpaper is working as well.

:heavy_check_mark: ***NOTE*** In some rare cases the logos did not show immediately. Try to reload the page by pressing Shift while clicking on the reload button. Sometimes the browser cache has to be wiped or a refresh of the company proxy server is necessary, if it's configured for too aggressive caching.
