# Use Case Documentation

This folder contains the documentation of all Use Cases and scenarios.

:warning: ***WARNING*** Be aware that although the demo environment is fully functionally and you can indeed order and deploy resources, deployment times are slower due to the technology used under the hood. Keep this in mind when demoing and do not waste time by waiting for an provisioning order to fully complete.

:heavy_check_mark: ***NOTE*** It is highly recommend to test the use cases you want to demonstrate in advance, Make yourself familiar with the environment and how things are implemented. Do not try to diverse into something you never checked yourself because you might confuse your audience.

## Customer Logo and Wallpaper

A must for every customer facing demo is to upload and active the customer logo and a wallpaper.

[Details about UI customization](ui-customization.md)

## Service Catalog

An example Service Catalog is populated with serveral Service Catalog Items.

[Details about the Service Catalog](service-catalog.md)

## Ansible Tower

As part of the lab environment an Ansible Tower is available. It is preconfigured with some Job Templates.

[Details about Ansible Tower](ansible-tower.md)

## Compliance Check and Enforcement

Examples can be found in the [Control Policies](../Control-Policies/) folder.

[Details about the example Control and Compliance Policies](../Control-Policies/README.md)

## Reporting

A few CloudForms reports can be found in the [Reports](../Reports/) sub folder.

[Details about the example Reports](../Reports/README.md)

## CMDB

CloudForms does not aim to replace a CMDB solution, however we can use custom attributes to add additional data and associate it to a given object. Custom attributes are shown in the UI if defined. They can also be utilized in Reports.

[Details about the Custom Attribute use case](custom-attributes.md)

## MIQ Expression for Dynamic Dialogs

The Automate Domain in the [../Automate](../Automate/) Sub folder has some examples for the new Expression Methods introduced with CloudForms 4.6.

You can find more details about how to create and use them in Peter McGowan's [Mastering Automation Addendum for CloudForms 4.6 and ManageIQ Gaprindashvili
](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/chapter) book.