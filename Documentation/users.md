# Users, Groups and Roles

## Simple End User

* Username: user
* Password: default password
* Group: enduser
* Role: enduser, this role provides mostly read only access with limited edit/modify privileges

## Quota Demo User

* Username: quota
* Password: default password
* Group: quota
* Role: enduser, this role provides mostly read only access with limited edit/modify privileges

This user is ready to use for the quota use cases. There are already VMs deployed which are owned by this user, so provisioning will fail due to quota exceeded.