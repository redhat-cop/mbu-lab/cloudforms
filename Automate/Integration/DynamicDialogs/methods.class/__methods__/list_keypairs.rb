#
#            Automate Method
#
# Copyright (C) 2016, Christian Jung
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

$evm.log("info", "EVM Automate Method Started")

# Dump all of root's attributes to the log
$evm.root.attributes.sort.each { |k, v| $evm.log("info", "Root:<$evm.root> Attribute - #{k}: #{v}")}

key_pairs={}

$evm.vmdb("ext_management_system").all.each { |ems| 
    $evm.log("info","Current provider: #{ems.name} with Provider type: #{ems.type}")
    next unless ems.type=="ManageIQ::Providers::Openstack::CloudManager"
    next if ems.key_pairs.nil?
    ems.key_pairs.each { |key_pair| 
        $evm.log("info", "Current key pair: #{key_pair.inspect}")
        name = key_pair.name
        key_pairs[name]=name
    }
}

$evm.log("info", "Cloud Networks: #{cloud_networks.inspect}")

dialog_field = $evm.object

#dialog_field["required"] = "true"
#dialog_field["protected"] = "false"
#dialog_field["read_only"] = "false"
dialog_field["values"] = key_pairs

#
# Exit method
#
$evm.log("info", "EVM Automate Method Ended")
exit MIQ_OK
